import { addLetter, backspace, commit, isLetter, setLoading } from "./buisnessFile.mjs"
async function init() {

    document.addEventListener('keydown', function handleKeyPress(event) {
        let done = false;
        let isLoading = false;
        if (done == true || isLoading == true) {
            return alert("Refresh It");
        }
        const action = event.key;

        if (action === "Enter") {
            setLoading()
            commit();
        } else if (action === 'Backspace') {
            backspace();
        } else if (isLetter(action)) {
            addLetter(action.toUpperCase());
        } else {
            return alert("Not relevant key");
        }
    })

}

init();