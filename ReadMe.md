# Word Masters

As name suggest this project is a game of words in which you have to guess the exact word which is has been fetched through an API.

# Project Description

* Take the 5 letter word and check if it is correct or not
* You will get 5 Chances to guess the right word 
* If the guess word is not correct, but some of the letters is correct it will change the colour into yellow.
* If the guess word is not correct, but some of letter is correct and its in the right place also background color will change into green or white.

# Tech Stack

* HTML
* CSS
* JavaScript

# The challenges

* To handle the async functions
* Loading spiral which should be hidden after loading of the whole page 
* The Row should not change before pressing the Enter button. After enter it check the word and if the word is not relevant it will change the background color into red.

# Project Link

https://sweet-peony-f91767.netlify.app/

