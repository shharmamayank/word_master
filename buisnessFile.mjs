const letters = document.querySelectorAll(".card-1-1");
const loadingDiv = document.querySelector(".info-bar");
const answer_letter = 5;
let Rounds = 6;
let currentRow = 0;
let currentLetter = '';
let done = false;
let isLoading = true;
isLoading = false;
setLoading(isLoading);
function addLetter(value) {
    if (currentLetter.length < answer_letter) {
        currentLetter += value

    } else {
        currentLetter = currentLetter.substring(0, currentLetter.length - 1) + value

    }
    letters[currentRow * answer_letter + currentLetter.length - 1].innerText = value
}
function backspace() {
    currentLetter = currentLetter.substring(0, currentLetter.length - 1)
    letters[currentRow * answer_letter + currentLetter.length].innerText = "";
}

async function commit() {
    const response = await fetch("https://words.dev-apis.com/word-of-the-day");
    const responseObject = await response.json();
    const word = responseObject.word.toUpperCase();
    console.log(word)
    let wordParts = word.split("");


    if (currentLetter.length !== answer_letter) {
        return alert("Fill all the boxes");
    }

    const guessWord = currentLetter.split('');
    const map = makeMap(wordParts)
    let allRight = true;
    for (let index = 0; index < answer_letter.length; index++) {
        if (guessWord[index] === wordParts[index]) {
            letters[currentRow * answer_letter + index].classList.add("correct");
            map[guessWord[index]]--;
        }
    }
    for (let index = 0; index < answer_letter; index++) {
        if (guessWord[index] === wordParts[index]) { }
        else if (map[guessWord[index]] && map[guessWord[index]] > 0) {
            allRight = false;
            letters[currentRow * answer_letter + index].classList.add("close");
            map[guessWord[index]]--;
        } else {
            allRight = false;
            letters[currentRow * answer_letter + index].classList.add("wrong");
        }
    }
    currentRow++;
    currentLetter = '';
    if (allRight) {
        alert("you win");
        document.querySelector(".brand").classList.add("winner");
        done = true;
    } else if (currentRow === Rounds) {
        alert(`you lose, the word was ${word}`);
        done = true;
    }

}
function isLetter(letter) {
    return /^[a-zA-Z]$/.test(letter)

}
function setLoading(isLoading) {
    loadingDiv.classList.toggle("hidden", !isLoading);
}
function makeMap(array) {
    const obj = {};
    for (let i = 0; i < array.length; i++) {
        if (obj[array[i]]) {
            obj[array[i]]++;
        } else {
            obj[array[i]] = 1;
        }
    }
    return obj;
}
export { addLetter, backspace, commit, isLetter, setLoading }